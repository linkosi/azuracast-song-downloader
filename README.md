# Azuracast Song Downloader
Script for download songs from "SoundCloud" and "Spotify".

## Getting started
Requirements:

```
sudo apt install python3-pip ffmpeg youtube-dl
pip3 install scdl
pip3 install spotify_dl
```

bashrc config:

```
vim ~/.bashrc
alias scdl='~/.local/bin/scdl'
alias spot='~/.spotify_dl'
```

### Authentication

#### SoundCloud
* Find your OAuth token by visiting SoundCloud after logging in and watching any of the browsers requests to the SoundCloud API, the token will be under the `Authorization` header of any of these requests
* Place OAuth token in the `~/.config/scdl/scdl.cfg`


#### Spotify
* Login to Spotify developer console and click on "Create an App". Fill in details for name and description
* Make a note of Client ID and Client Secret. These values need to be then set `SPOTIPY_CLIENT_ID`, `SPOTIPY_CLIENT_SECRET` environment variables respectively.
You can set environment variables as mentioned below:

```
vim ~/.bash_profile
export SPOTIPY_CLIENT_ID=your-spotify-client-id
export SPOTIPY_CLIENT_SECRET=your-spotify-client-secret
source ~/.bash_profile
```